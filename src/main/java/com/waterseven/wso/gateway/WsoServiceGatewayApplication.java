package com.waterseven.wso.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WsoServiceGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsoServiceGatewayApplication.class, args);
	}

}
